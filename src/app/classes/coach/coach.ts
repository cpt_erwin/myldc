import { CoachImage } from "./coach-image";
import { CoachTag } from "./coach-tag";
import { CoachEquipment } from "./coach-equipment";

export class Coach {
    name: string;
    images: CoachImage[];
    tags: CoachTag[];
    equipment: CoachEquipment[];

    constructor(name: string, images: CoachImage[], tags: CoachTag[], equipment: CoachEquipment[]) {
        this.name = name;
        this.images = images;
        this.tags = tags;
        this.equipment = equipment;
    }

    splitTagsToPairs() {
        return this.tags.reduce(function (result, value, index, array) {
            if (index % 2 === 0)
                result.push(array.slice(index, index + 2));
            return result;
        }, []);
    }
}