export class CoachEquipment {

    static readonly CD_DVD = new CoachEquipment("CD / DVD", "/src/assets/images/cd-dvd.png", "CD / DVD");
    static readonly MICROPHONE = new CoachEquipment("Microphone", "/src/assets/images/mikrofon.png", "Microphone");
    static readonly AIR_CONDITIONER = new CoachEquipment("Air conditioner", "/src/assets/images/klimatizace.png", "Air conditioner");
    static readonly TOILET = new CoachEquipment("Toilet", "/src/assets/images/toaleta.png", "Toilet");
    static readonly SEAT_BELTS = new CoachEquipment("Seat belts", "/src/assets/images/bezp-pasy.png", "Seat belts");
    static readonly FRIDGE = new CoachEquipment("Fridge", "/src/assets/images/chladnicka.png", "Fridge");

    name: string;
    icon: string; // Shouldn't this be class Icon?
    tooltip: string;

    constructor(name: string, icon: string, tooltip: string) {
        this.name = name;
        this.icon = icon;
        this.tooltip = tooltip;
    }
}
