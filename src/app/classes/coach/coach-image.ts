export class CoachImage {
    src: string;

    constructor(src: string) {
        this.src = src;
    }
}