export class CoachTag {

    static readonly YEAR_OF_MANUFACTURE = "Year of manufacture";
    static readonly PLATE_NUMBER = "Plate number";
    static readonly LENGTH = "Length";
    static readonly NUMBER_OF_SEATS = "Number of seats";
    static readonly CLASS = "Class";
    static readonly MODEL = "Model";
    static readonly REG = "VRN";
    static readonly COLOR = "Color";
    static readonly EMISSIONS = "Emissions";

    name: string;
    value: string;

    constructor(name: string, value: string) {
        this.name = name;
        this.value = value;
    }
}