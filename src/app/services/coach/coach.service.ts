import { Injectable } from '@angular/core';

import { Coach } from 'src/app/classes/coach/coach';
import { CoachImage } from 'src/app/classes/coach/coach-image';
import { CoachEquipment } from 'src/app/classes/coach/coach-equipment';
import { CoachTag } from 'src/app/classes/coach/coach-tag';

@Injectable({
  providedIn: 'root'
})
export class CoachService {

  coaches = [
    new Coach(
      "MAN",
      [
        new CoachImage("/src/assets/images/kr1gm03-01.jpg"),
        new CoachImage("/src/assets/images/kr1gm03-02.jpg"),
        new CoachImage("/src/assets/images/kr1gm03-03.jpg"),
        new CoachImage("/src/assets/images/kr1gm03-04.jpg")
      ],
      [
        new CoachTag(CoachTag.MODEL, "LION’S COACH"),
        new CoachTag(CoachTag.YEAR_OF_MANUFACTURE, "2018"),
        new CoachTag(CoachTag.REG, "PL"),
        new CoachTag(CoachTag.COLOR, "White"),
        new CoachTag(CoachTag.NUMBER_OF_SEATS, "49+2"),
        new CoachTag(CoachTag.EMISSIONS, "EURO 6"),
      ],
      [
        CoachEquipment.AIR_CONDITIONER,
        CoachEquipment.MICROPHONE,
        CoachEquipment.CD_DVD,
        CoachEquipment.TOILET,
        CoachEquipment.SEAT_BELTS,
        CoachEquipment.FRIDGE,
      ]
    ),
    new Coach(
      "MERCEDES BENZ",
      [
        new CoachImage("/src/assets/images/nr640jt-01.jpg"),
        new CoachImage("/src/assets/images/nr640jt-02.jpg"),
        new CoachImage("/src/assets/images/nr640jt-03.jpg"),
        new CoachImage("/src/assets/images/nr640jt-04.jpg")
      ],
      [
        new CoachTag(CoachTag.MODEL, "TOURISMO"),
        new CoachTag(CoachTag.YEAR_OF_MANUFACTURE, "2018"),
        new CoachTag(CoachTag.REG, "CZ"),
        new CoachTag(CoachTag.COLOR, "White"),
        new CoachTag(CoachTag.NUMBER_OF_SEATS, "49+2"),
        new CoachTag(CoachTag.EMISSIONS, "EURO 6"),
      ],
      [
        CoachEquipment.AIR_CONDITIONER,
        CoachEquipment.MICROPHONE,
        CoachEquipment.CD_DVD,
        CoachEquipment.TOILET,
        CoachEquipment.SEAT_BELTS,
        CoachEquipment.FRIDGE,
      ]
    ),
    new Coach(
      "MERCEDES BENZ",
      [
        new CoachImage("/src/assets/images/m-benz-tourismo-0316-03.jpg"),
        new CoachImage("/src/assets/images/m-benz-tourismo-0316-02.jpg"),
        new CoachImage("/src/assets/images/m-benz-tourismo-0316-01.jpg")
      ],
      [
        new CoachTag(CoachTag.MODEL, "TOURISMO"),
        new CoachTag(CoachTag.YEAR_OF_MANUFACTURE, "2017"),
        new CoachTag(CoachTag.REG, "SK"),
        new CoachTag(CoachTag.COLOR, "White"),
        new CoachTag(CoachTag.NUMBER_OF_SEATS, "49+2"),
        new CoachTag(CoachTag.EMISSIONS, "EURO 6"),
      ],
      [
        CoachEquipment.AIR_CONDITIONER,
        CoachEquipment.MICROPHONE,
        CoachEquipment.CD_DVD,
        CoachEquipment.TOILET,
        CoachEquipment.SEAT_BELTS,
        CoachEquipment.FRIDGE,
      ]
    )
  ];

  constructor() {
    /*
    for (var i = 0; i < 35; i++) {
      this.coaches.push(
        new Coach(
          "Coach",
          [
            new CoachImage("https://picsum.photos/600/400?image=" + i),
            new CoachImage("https://picsum.photos/200/200?image=" + i),
            new CoachImage("https://picsum.photos/200/200?image=" + i),
            new CoachImage("https://picsum.photos/200/200?image=" + i)
          ],
          [
            new CoachTag(CoachTag.MODEL, "xxxxx"),
            new CoachTag(CoachTag.YEAR_OF_MANUFACTURE, "xxxx"),
            new CoachTag(CoachTag.REG, "XXX XXXX"),
            new CoachTag(CoachTag.COLOR, "xxxxx"),
            new CoachTag(CoachTag.NUMBER_OF_SEATS, "xx"),
            new CoachTag(CoachTag.EMISSIONS, "XXXX X")
          ],
          [
            CoachEquipment.AIR_CONDITIONER,
            CoachEquipment.MICROPHONE,
            CoachEquipment.CD_DVD,
            CoachEquipment.TOILET,
            CoachEquipment.SEAT_BELTS,
            CoachEquipment.FRIDGE
          ]
        )
      );
    }
    */
  }

  getNumberOfPages(entriesPerPage: number) {
    return Math.ceil(this.coaches.length / entriesPerPage);
  }

  getCoaches(entriesPerPage: number = 1, page: number = 0) {
    --page;
    return this.coaches.slice(page * entriesPerPage, (page * entriesPerPage) + entriesPerPage);
  }

}
