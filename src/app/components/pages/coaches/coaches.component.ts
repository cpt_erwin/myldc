import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Coach } from 'src/app/classes/coach/coach';
import { CoachService } from 'src/app/services/coach/coach.service';

import * as $ from 'jquery';
import bulmaCarousel from 'bulma-carousel';

import PhotoSwipe from 'photoswipe';
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default';

@Component({
  selector: 'app-coaches',
  templateUrl: './coaches.component.html',
  styleUrls: ['./coaches.component.scss']
})
export class CoachesComponent implements OnInit {

  numberOfPages: number = null;
  pages: number[] = [];
  activePage: number = null;
  coaches: Coach[] = [];
  coachesPerPage: number = 5;
  pswpElement = document.getElementById("pswp");

  constructor(private route: ActivatedRoute, private router: Router, private _coachService: CoachService) {
    this.numberOfPages = this._coachService.getNumberOfPages(this.coachesPerPage);
    this.pages = Array.from({ length: this.numberOfPages }).map((element, index) => {
      return index + 1;
    });

    // Prevent that url/:page load everytime it changes
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    // console.warn("ngOnInit");

    // FIXME: Make sure there's only values from 0 to max number of pages
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.activePage = parseInt(params.get('page')) || 1;
    });

    this.coaches = this._coachService.getCoaches(this.coachesPerPage, this.activePage);
  }

  ngAfterViewInit() {
    $(function () {
      // console.warn("ngAfterViewInit");
      var carousels = bulmaCarousel.attach(); // carousels now contains an array of all Carousel instances
    });
  }

  splitToGroups(array: any[], groupArrayLenght: number) {
    return array.reduce(function (result, value, index, originalArray) {
      if (index % groupArrayLenght === 0)
        result.push(originalArray.slice(index, index + groupArrayLenght));
      return result;
    }, []);
  }

  onClick(activeImageSrc, i) {
    var images = document.getElementById("img_container_" + i).getElementsByTagName("img");

    var options = { index: 0 };

    var imagesArray = [];
    var j = 0;
    for (let image of <any>images) {
      imagesArray.push({
        src: image.src,
        w: image.naturalWidth,
        h: image.naturalHeight
      });

      if (image.src == activeImageSrc) {
        options.index = j;
      }

      j++;
    }

    // Initializes and opens PhotoSwipe
    const gallery = new PhotoSwipe(document.querySelectorAll('.pswp')[0], PhotoSwipeUI_Default, imagesArray, options);
    gallery.init();
  }

}
