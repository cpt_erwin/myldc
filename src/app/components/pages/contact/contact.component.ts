import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  offerRequestForm: FormGroup;

  file = null;

  get fullName() {
    return this.offerRequestForm.get('fullName');
  }

  get email() {
    return this.offerRequestForm.get('email');
  }

  get fileName() {
    return this.offerRequestForm.get('fileName');
  }

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.offerRequestForm = this.formBuilder.group({
      fullName: [null, [Validators.required]], //TODO: Add regex for full name
      email: [null, [Validators.required, Validators.pattern('^([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_.-]+)\\.([a-zA-Z]{2,5})$')]],
      fileName: [null, [Validators.required]]
    });
  }

  onFileChange(event) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.file = reader.result;

        // need to run CD since file load runs outside of zone
        // this.cd.markForCheck();
      };
    }
  }

  onSubmit() {
    if (this.offerRequestForm.valid) {
      
      $('#form').hide();
      $('#loader').show();
      $('#fileError').hide();

      // TODO: Create fake API for testing
      return this.http.post('https://www.myldc.eu/api/mailer/index.php', {fullName: this.offerRequestForm.value.fullName, email: this.offerRequestForm.value.email, file: this.file }).subscribe(
        data => {
          // console.log("POST Request is successful ", data);
          $('.circle-loader').toggleClass('load-complete');
          $('.checkmark').toggle();
        },
        error => {
          // console.log("Error", error);
          $('#loader').hide();
          $('#form').show();

          if (error.error.text.search("/Unsupported file type/gi")) {
            $('#fileError').show();
          }
        }
      );

    } else {
      // console.error("Form submitted but invalid!");
    }
  }

}