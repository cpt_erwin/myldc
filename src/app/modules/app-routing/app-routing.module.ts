import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './../../components/pages/home/home.component';
import { AboutComponent } from './../../components/pages/about/about.component';
import { ContactComponent } from './../../components/pages/contact/contact.component';
import { CoachesComponent } from './../../components/pages/coaches/coaches.component';

import { PageNotFoundComponent } from './../../components/pages/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'coaches',
    component: CoachesComponent
  },
  {
    path: 'coaches/:page',
    component: CoachesComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: '**',
    redirectTo: '/'
    // component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }